'use strict';

export const names = {
  W: 'W',
  M: 'M',
  X: 'X',
  O: 'O',
  Z: 'Z',
  HALT: 'H',
  LEFT: 0,
  RIGHT: 1,
  UP: 2,
  DOWN: 3
};
