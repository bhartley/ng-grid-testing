'use strict';

import {names} from './constantNames';

export const rules = {
  'W': {
    action: names.X,
    color: 'orange',
    moves: [
      names.LEFT, names.DOWN
    ]
  },
  'M': {
    action: names.Z,
    color: 'red',
    moves: [
      names.RIGHT
    ]
  },
  'X': {
    action: names.O,
    color: 'black',
    moves: [
      names.UP, names.LEFT
    ]
  },
  'O': {
    action: names.M,
    color: 'green',
    moves: [
      names.UP
    ]
  },
  'Z': {
    action: names.HALT,
    color: 'yellow',
    moves: []
  },
  'H': {
    color: 'pink'
  }
};
