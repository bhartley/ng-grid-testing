import {Component, OnInit} from '@angular/core';
import {rules} from './rules';
import {names} from './constantNames';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  halted = false;

  defaultXSize = 10;
  defaultYSize = 10;
  defaultVal = 'W';

  minXSize: number;
  maxXSize: number;
  maxXPos: number;
  minYSize: number;
  maxYSize: number;
  maxYPos: number;

  grid: any[];
  htmlTable: any;

  position = {
    x: Math.floor(this.defaultXSize / 2),
    y: Math.floor(this.defaultYSize / 2)
  };

  ngOnInit() {
    this.grid = this.newGrid(this.defaultXSize, this.defaultYSize, this.defaultVal);
    this.htmlTable = document.createElement('table');
    document.body.appendChild(this.htmlTable);
    this.showGrid();
  }

  reset() {
    this.halted = false;
    this.grid = this.newGrid(this.defaultXSize, this.defaultYSize, this.defaultVal);
    this.showGrid();
  }

  newGrid(x: number, y: number, val: string): any[] {
      const grid = [];
      const row = [];
      this.minXSize = 0;
      this.maxXSize = x;
      this.maxXPos = this.maxXSize - 1;
      this.minYSize = 0;
      this.maxYSize = y;
      this.maxYPos = this.maxYSize - 1;

      for (let i = 0; i < x; i++) {
        row.push(val);
      }
      for (let j = 0; j < y; j++) {
        grid.push(row.slice());
      }

      return grid;
    }

  getCurrentValue(): string {
    return this.grid[this.position.y][this.position.x];
  }

  setValue(val: string): void {
    this.grid[this.position.y][this.position.x] = val;
  }

  advance(steps: number, delay: number): void {
    const that = this;
    for (let i = 0; i < steps; i++) {
      if (this.halted) break;
      setTimeout(function() {
        that.advanceOne();
      }, delay * i);
    }
  }

  advanceOne(): void {
    if (this.halted) {
      return;
    }

    const currentVal = this.getCurrentValue();
    if (rules[currentVal].action === names.HALT) {
      this.halted = true;
      this.setValue(names.HALT);
      this.showGrid();
      return;
    }

    this.setValue(rules[currentVal].action);
    const moves = rules[currentVal].moves;

    for (let i = 0; i < moves.length; i++) {
      if (moves[i] === names.LEFT) {
        this.position.x -= 1;
        if (this.position.x < this.minXSize) this.position.x = this.maxXPos;
      } else if (moves[i] === names.RIGHT) {
        this.position.x += 1;
        if (this.position.x > this.maxXPos) this.position.x = this.minXSize;
      } else if (moves[i] === names.UP) {
        this.position.y -= 1;
        if (this.position.y < this.minYSize) this.position.y = this.maxYPos;
      } else if (moves[i] === names.DOWN) {
        this.position.y += 1;
        if (this.position.y > this.maxYPos) this.position.y = this.minYSize;
      }
    }
    this.showGrid();
  }

  showGrid(): void {
    const newTable = document.createElement('table');
    document.body.appendChild(newTable);
    document.body.removeChild(this.htmlTable);
    for (let i = this.minYSize; i < this.maxYSize; i++) {
      const tr = document.createElement('tr');
      for (let j = this.minXSize; j < this.maxXSize; j++) {
        const td = document.createElement('td');
        const val = document.createElement('span');
        val.innerHTML = this.grid[i][j].fontcolor(rules[this.grid[i][j]].color);
        td.appendChild(val);
        tr.appendChild(td);
      }
      newTable.appendChild(tr);
    }
    this.htmlTable = newTable;
  }

  advanceClick(steps: number, delay: number): void {
    if (!this.halted) this.advance(steps, delay);
  }

  setXY(x: number, y: number) {
    this.grid = this.newGrid(x, y, this.defaultVal);
    this.showGrid();
  }
}
